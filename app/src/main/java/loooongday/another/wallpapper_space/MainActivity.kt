package loooongday.another.wallpapper_space

import android.Manifest
import android.app.WallpaperManager
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Environment
import android.support.v4.app.ActivityCompat
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import android.support.v4.content.ContextCompat
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import com.google.gson.internal.LinkedTreeMap
import com.squareup.picasso.Picasso
import com.squareup.picasso.Target
import kotlinx.android.synthetic.main.activity_main.*
import loooongday.another.wallpapper_space.api.MyApi
import loooongday.another.wallpapper_space.fragments.BlankFragment
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.File
import java.io.FileOutputStream
import java.io.IOException

class MainActivity : AppCompatActivity() {
    private val items = ArrayList<String>()
    private lateinit var adapter: SimpleAdapter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayShowTitleEnabled(false)
        load()
        adapter = SimpleAdapter(getSupportFragmentManager())
        viewpager.adapter=adapter
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    private val api_key: String = "7495fe927ba7cdf8d1c5f9870c4b20de9786475be3732d4a5f2c3d923da06ec9"

    private fun load() {
        MyApi.create().getImgList(api_key, "100").enqueue(object : Callback<List<HashMap<String, Any>>> {
            override fun onFailure(call: Call<List<HashMap<String, Any>>>?, t: Throwable?) {
                Log.d("fail", "fail")
            }

            override fun onResponse(call: Call<List<HashMap<String, Any>>>?, response: Response<List<HashMap<String, Any>>>) {
                val objects = ArrayList<HashMap<String, Any>>(response.body())
                Log.d("kek", objects.toString())
                for (obj in objects)
                items.add((obj["urls"]as LinkedTreeMap<String, String>)["regular"].toString() + "/1080x1920")

                adapter.notifyDataSetChanged()
                //images.add(obj.urls.regular + "/1080x1920")
                    //Log.d("id", (obj["urls"] as LinkedTreeMap<String, String>)["regular"].toString())

                //adapter.notifyDataSetChanged()
            }
        })
    }

    inner class SimpleAdapter(fragmentManager: FragmentManager) : FragmentPagerAdapter(fragmentManager) {
        override fun getItem(position: Int): Fragment = BlankFragment.newInstance(items[position])

        override fun getCount(): Int =
                items.size

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId)
        {
            R.id.download -> {
                download()
                return true
            }
            R.id.wallpper -> {
                wallpper()
                return true
            }
            else -> return false
        }
    }

    private fun wallpper() {

        Picasso.with(this).load(items[viewpager.currentItem]).into(object : Target {
            override fun onBitmapFailed(errorDrawable: Drawable?) {
                Log.d("kek", "failur")

            }

            override fun onBitmapLoaded(bitmap: Bitmap, from: Picasso.LoadedFrom) {
                val myWallpaperManager = WallpaperManager.getInstance(applicationContext)
                try {
                    myWallpaperManager.setBitmap(bitmap)
                } catch (e: IOException) {
                    e.printStackTrace()
                }
            }

            override fun onPrepareLoad(placeHolderDrawable: Drawable?) {
                Log.d("kek", "prepare")
            }

        })
    }

    private fun download() {

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE),
                    50)
        }
        Picasso.with(this).load(items[viewpager.currentItem]).into(object : Target {
            override fun onBitmapFailed(errorDrawable: Drawable?) {
                Log.d("kek", "failur")

            }

            override fun onBitmapLoaded(bitmap: Bitmap, from: Picasso.LoadedFrom) {
                Thread(Runnable {
                    var file = File(Environment.getExternalStorageDirectory(), getString(R.string.app_name))
                    if (!file.exists()) {
                        file.mkdirs()
                    }
                    file = File(Environment.getExternalStorageDirectory().getPath() + "/" + getString(R.string.app_name) + "/image" + viewpager.currentItem + ".jpg")
                    try {
                        file.createNewFile()
                        val ostream = FileOutputStream(file)
                        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, ostream)
                        ostream.flush()
                        ostream.close()
                        Log.d("succes", items[viewpager.currentItem])
                        runOnUiThread {
                            Toast.makeText(this@MainActivity, "Файл загружен", Toast.LENGTH_LONG).show()

                        }

                    } catch (e: IOException) {
                        Log.d("IOException", e.getLocalizedMessage())
                    }
                }).start()

            }

            override fun onPrepareLoad(placeHolderDrawable: Drawable?) {
                Log.d("kek", "prepare")
            }
        })
    }
}
